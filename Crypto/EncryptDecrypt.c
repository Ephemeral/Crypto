//
//  EncryptDecrypt.c
//  Crypto
//
//  Created by Ephemeral on 09.08.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#include "EncryptDecrypt.h"
uint8_t ReadBinary(const char* Name,uint8_t* Buffer,size_t Size)
{
    FILE* Handle = fopen(Name, "rb");
    if(!Handle)
        return 0;
    
    size_t Read = fread(Buffer, sizeof(uint8_t), Size, Handle);
    
    if(Read != Size)
        return 0;
    
    return 1;
}
size_t GetFileSize(const char* Name)
{
    size_t Size = 0;
    
    FILE* Handle = fopen(Name, "rb");
    if(!Handle)
        return 0;

    fseek(Handle,0, SEEK_END);
    
    Size = ftell(Handle);
    
    fclose(Handle);
    
    
    return Size;
}
uint8_t CryptoEncrypt(const char* PublicKeyName,const char* Input,const char* output)
{
    uint8_t Status = 0;
    uint8_t* Buffer = 0;
    uint8_t* Encrypted = 0;
    size_t KeySize = 0;
    size_t FileSize = 0;
    
    CryptoContext Context = {0};
    
    
    CreateCryptoContext(&Context);
    
    if (mbedtls_ctr_drbg_seed(&Context.CtrContext, mbedtls_entropy_func, &Context.EntropyContext, NULL, 0))
        goto Cleanup;
    
    KeySize = GetFileSize(PublicKeyName);
    
    if(!KeySize)
        goto Cleanup;
    
    FileSize = GetFileSize(Input);
    
    if(!FileSize)
        goto Cleanup;
    
    Buffer = (uint8_t*)malloc(KeySize);
    
    if(!Buffer)
        goto Cleanup;
    
    
    
    if(!ReadBinary(PublicKeyName, Buffer, KeySize))
        goto Cleanup;
    
    
    	
    if (mbedtls_mpi_read_binary(&Context.RsaContext.N, Buffer, KeySize))
        goto Cleanup;
    
    Context.RsaContext.len = (mbedtls_mpi_bitlen(&Context.RsaContext.N) + 7) >> 3;
    
    if (mbedtls_mpi_read_binary(&Context.RsaContext.E,PublicExponent, sizeof(PublicExponent)))
        goto Cleanup;
    
    
    memset(Buffer, 0, KeySize);
    
    if(!ReadBinary(Input, Buffer, FileSize))
        goto Cleanup;
    
    Encrypted = (uint8_t*)malloc(Context.RsaContext.len);
    
    if(!Encrypted)
        goto Cleanup;
    
    if (mbedtls_rsa_pkcs1_encrypt(&Context.RsaContext, mbedtls_ctr_drbg_random, &Context.CtrContext, MBEDTLS_RSA_PUBLIC, FileSize, Buffer, Encrypted))
        goto Cleanup;
    
    if(!WriteBinaryToFile(output, Encrypted, Context.RsaContext.len))
        goto Cleanup;
    
    
    Status = 1;
    
    
    
Cleanup:
    DestroyCryptoContext(&Context);
    
    if(Buffer)
    {
        free(Buffer);
        Buffer = 0;
    }
    if(Encrypted)
    {
        free(Encrypted);
        Encrypted = 0;
    }
    
    return Status;
}



uint8_t CryptoDecrypt(const char* Input,const char* Output,size_t* DecryptedSize)
{
    uint8_t Status = 0;
    size_t KeySize = 0;
    uint8_t* Buffer = 0;
    uint8_t* Decrypted = 0;
    
    CryptoContext Context = {0};
    CreateCryptoContext(&Context);
    
    if (mbedtls_ctr_drbg_seed(&Context.CtrContext, mbedtls_entropy_func, &Context.EntropyContext, NULL, 0))
        goto Cleanup;
    
    KeySize = GetFileSize("Key/PublicKey");
    
    if(!KeySize)
        goto Cleanup;
    
    Buffer = (uint8_t*)malloc(KeySize);
    if(!Buffer)
        goto Cleanup;
    
    
    
    if(!ReadBinary("Key/PublicKey", Buffer, KeySize))
        goto Cleanup;
    
    if (mbedtls_mpi_read_binary(&Context.RsaContext.N, Buffer, KeySize))
        goto Cleanup;
    
    Context.RsaContext.len = (mbedtls_mpi_bitlen(&Context.RsaContext.N) + 7) >> 3;
    
    if (mbedtls_mpi_read_binary(&Context.RsaContext.E, PublicExponent, sizeof(PublicExponent)))
        goto Cleanup;
    
    if(!ReadBinary("Key/PrivateExponent", Buffer, Context.RsaContext.len))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.D, Buffer, Context.RsaContext.len))
        goto Cleanup;
    
    
    
    if(!ReadBinary("Key/FirstPrime", Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.P, Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    
    
   
    if(!ReadBinary("Key/SecondPrime", Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.Q, Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    
    
    
    if(!ReadBinary("Key/DModP", Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.DP, Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    
    
    
    if(!ReadBinary("Key/DModQ", Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.DQ, Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    
    
    
    if(!ReadBinary("Key/QModInverseP", Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    if(mbedtls_mpi_read_binary(&Context.RsaContext.QP, Buffer, Context.RsaContext.len/2))
        goto Cleanup;
    
    memset(Buffer, 0, KeySize);

    
   if(!ReadBinary(Input, Buffer, Context.RsaContext.len))
       goto Cleanup;
    
    Decrypted = (uint8_t*)malloc(Context.RsaContext.len);
    if(!Decrypted)
        goto Cleanup;
    
    memset(Decrypted, 0, Context.RsaContext.len);
    int32_t Ret = mbedtls_rsa_pkcs1_decrypt(&Context.RsaContext, mbedtls_ctr_drbg_random, &Context.CtrContext, MBEDTLS_RSA_PRIVATE, DecryptedSize, Buffer, Decrypted, Context.RsaContext.len);
    if (Ret)
    {
       // mbedtls_strerror(Ret,Decrypted,siz)
      //  printf("%X\n",Ret);
        goto Cleanup;
    }

    
    if(!WriteBinaryToFile(Output, Decrypted, *DecryptedSize))
        goto Cleanup;
    
    Status = 1;
Cleanup:
    DestroyCryptoContext(&Context);
    if(Buffer)
    {
        free(Buffer);
        Buffer = 0;
    }
    if(Decrypted)
    {
        free(Decrypted);
        Decrypted = 0;
    }
    return Status;
}
