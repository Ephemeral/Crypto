//
//  GenerateKey.c
//  Crypto
//
//  Created by Ephemeral on 09.08.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#include "GenerateKey.h"
void CreateCryptoContext(CryptoContext* Context)
{
    mbedtls_entropy_init(&Context->EntropyContext);
    mbedtls_ctr_drbg_init(&Context->CtrContext);
    mbedtls_rsa_init(&Context->RsaContext, MBEDTLS_RSA_PKCS_V15, 0);
}
void DestroyCryptoContext(CryptoContext* Context)
{
    mbedtls_rsa_free(&Context->RsaContext);
    mbedtls_entropy_free(&Context->EntropyContext);
    mbedtls_ctr_drbg_free(&Context->CtrContext);
}
uint8_t WriteBinaryToFile(const char* FileName,uint8_t* Buffer, size_t Size)
{
    size_t Written = 0;
    FILE* Handle = fopen(FileName, "wb");
    if(Handle == NULL)
        return 0;
    
    Written = fwrite(Buffer, sizeof(uint8_t), Size, Handle);
    
    fclose(Handle);
    
    if(Written != Size)
        return 0;
    
    return 1;
}
uint8_t GenerateKey(uint32_t KeySizeInBits)
{
    uint32_t KeySizeInBytes = KeySizeInBits/8;
    uint8_t* Key = NULL;
    uint8_t Status = 0;
    
    CryptoContext Context = {0};
    
    
    CreateCryptoContext(&Context);
    
    
    
    if((KeySizeInBytes & 1))
        goto CleanUp;
    
    if (mbedtls_ctr_drbg_seed(&Context.CtrContext, mbedtls_entropy_func, &Context.EntropyContext, NULL, 0))
        goto CleanUp;
    
    if (mbedtls_rsa_gen_key(&Context.RsaContext, mbedtls_ctr_drbg_random, &Context.CtrContext, KeySizeInBits, CRYPTO_PUBLIC_EXPONENT))
        goto CleanUp;
    
    
    Key = (uint8_t*)malloc(KeySizeInBytes);
    memset(Key, 0, KeySizeInBytes);
    
    if(!Key)
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.D, Key, KeySizeInBytes))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/PrivateExponent", Key, KeySizeInBytes))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.N, Key, KeySizeInBytes))
        goto CleanUp;
    
    
    if(!WriteBinaryToFile("Key/PublicKey", Key, KeySizeInBytes))
        goto CleanUp;
    
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.E, Key, sizeof(uint32_t)))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/PublicExponent", Key, sizeof(uint32_t)))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.P, Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/FirstPrime", Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.Q, Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/SecondPrime", Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.DP, Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/DModP", Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.DQ, Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/DModQ", Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if (mbedtls_mpi_write_binary(&Context.RsaContext.QP, Key, KeySizeInBytes/2))
        goto CleanUp;
    
    if(!WriteBinaryToFile("Key/QModInverseP", Key, KeySizeInBytes/2))
        goto CleanUp;
    
    Status = 1;
CleanUp:
    
    DestroyCryptoContext(&Context);
    
    if(Key)
    {
        free(Key);
        Key = NULL;
    }
    
    return Status;
}
