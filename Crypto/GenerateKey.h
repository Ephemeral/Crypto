//
//  GenerateKey.h
//  Crypto
//
//  Created by Ephemeral on 09.08.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#ifndef GenerateKey_h
#define GenerateKey_h

#include <stdlib.h>
#include <string.h>
#include <mbedtls/include/rsa.h>
#include <mbedtls/include/ctr_drbg.h>
#include <mbedtls/include/entropy.h>
#include <mbedtls/include/error.h>
#define CRYPTO_PUBLIC_EXPONENT 65537
#include "PublicExponent.h"
typedef struct _CryptoContext
{
    mbedtls_rsa_context RsaContext;
    mbedtls_ctr_drbg_context CtrContext;
    mbedtls_entropy_context EntropyContext;
}CryptoContext;

uint8_t WriteBinaryToFile(const char* FileName,uint8_t* Buffer, size_t Size);

void CreateCryptoContext(CryptoContext* Context);

void DestroyCryptoContext(CryptoContext* Context);

uint8_t GenerateKey(uint32_t KeySizeInBits);

#endif /* GenerateKey_h */
