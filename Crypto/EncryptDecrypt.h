//
//  EncryptDecrypt.h
//  Crypto
//
//  Created by Ephemeral on 09.08.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#ifndef EncryptDecrypt_h
#define EncryptDecrypt_h

#include "GenerateKey.h"

uint8_t CryptoEncrypt(const char* PublicKeyName,const char* Input,const char* output);

uint8_t CryptoDecrypt(const char* Input,const char* Output,size_t* DecryptedSize);

#endif /* EncryptDecrypt_h */
