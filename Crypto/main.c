//
//  main.c
//  Crypto
//
//  Created by Ephemeral on 09.08.17.
//  Copyright © 2017 Ephemeral. All rights reserved.
//

#include <stdio.h>
#include "EncryptDecrypt.h"
int main(int argc, const char * argv[])
{
    if(argc < 3)
    {
        puts("Example: Crypto gen 2048\nCrypto encrypt Key Input Output\nCrypto decrypt Input Output");
        return 1;
    }
    

    if(!strcmp(argv[1],"gen"))
    {
        uint32_t KeySize = atoi(argv[2]);
        if(!KeySize)
            return 1;
        if(GenerateKey(KeySize))
            puts("Key generated.");
        else
            puts("Error generating key.");
    }
    else if(!strcmp(argv[1],"decrypt"))
    {
        size_t DecryptedLength = 0;
        if(argc != 4)
        {
            puts("\nNot enough arguments to decrypt: Crypto decrypt Input Output");
            return 1;
        }
        if(CryptoDecrypt(argv[2], argv[3], &DecryptedLength))
            puts("File decrypted");
        else
            puts("Failed to edecrypt");
    }

    else if(!strcmp(argv[1],"encrypt"))
    {
        if(argc != 5)
        {
            puts("\nNot enough arguments to encrypt: Crypto encrypt key Input output\n");
            return 1;
        }
        if(CryptoEncrypt(argv[2], argv[3], argv[4]))
            puts("File Encrypted");
        else
            puts("Failed to encrypt");
    }
    
    return 0;
}
